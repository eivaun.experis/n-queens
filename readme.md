# The N-Queens problem
This is a solution to the n-queens problem.
The n-queens problem consists of placing n queens on a n*n board without any of them being able to attack each other, meaning that no two queens can share the same row, columns or diagonal.

## Backtracking
The solutions are based on recursively placing the queens and backtrack when a conflict is detected.

n-queens simply stores the entire board as a 2D vector.

n-queens-optimized only stores the column number the queen is placed in for each row. It also maintains lists for the number of queens that can attack a specific column and diagonals. Instead of searching rows and columns from left to right, it uses a random order.

## Greedy search
Greedy search is based on simply placing all the queens in the same column and then moving them one by one until no one can attack each other anymore.
For each iteration each queen is sequentially moved to the square in their row where the least amount of queens can attack them.
When every queen is placed in a square with 0 attacks a solution is found.

## Build
The project is built using CMake with the provided CMakeLists.txt

## Usage
```
./n-queens N
```
Where N is a number in the range [1-10]
```
./n-queens-optimized N
```
Where N is a number in the range [1-1000]
```
./n-queens-greedy-search N
```
Where N is a number in the range [1-10000]

## Contributors
Eivind Vold Aunebakk