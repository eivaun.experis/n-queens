// Based on https://sites.google.com/site/nqueensolver/home/algorithms/3systematic-greedy-algorithm

#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <mutex>
#include <numeric>

#include "utility.h"

// The required offset into the attacks_in_diagonal_neg vector as the diagonal is calculated as col-row and we don't want negative indices.
int neg_offset;

// The number of threads that have tried all their starting columns
int completed_threads = 0;

// The id of the first thread to complete
int first_complete_thread = -1;

// To avoid multiple threads reading/writing to completed_threads and first_complete_thread at once
std::mutex complete_lock;

// Gets the number of queens that can attack the given position
int get_num_attacks(int row, int col, std::vector<int>& attacks_in_col, std::vector<int>& attacks_in_diagonal_pos,
                    std::vector<int>& attacks_in_diagonal_neg)
{
    int attacks = 0;
    attacks += attacks_in_col[col];
    attacks += attacks_in_diagonal_pos[col + row];
    attacks += attacks_in_diagonal_neg[col - row + neg_offset];
    return attacks;
}

// Places a queen in the given positions and marking the column and diagonals it can attack. Assumes the position is free
void place_queen(int row, int col, std::vector<int>& queens, std::vector<int>& attacks_in_col,
                 std::vector<int>& attacks_in_diagonal_pos, std::vector<int>& attacks_in_diagonal_neg)
{
    queens[row] = col;
    attacks_in_col[col]++;
    attacks_in_diagonal_pos[col + row]++;
    attacks_in_diagonal_neg[col - row + neg_offset]++;
}

// Removes the queen in the given position. Assumes the position is occupied
void remove_queen(int row, int col, std::vector<int>& attacks_in_col, std::vector<int>& attacks_in_diagonal_pos,
                  std::vector<int>& attacks_in_diagonal_neg)
{
    attacks_in_col[col]--;
    attacks_in_diagonal_pos[col + row]--;
    attacks_in_diagonal_neg[col - row + neg_offset]--;
}

// Run for each thread
void solve(int thread_id, int n, int max_iterations, int min_col, int max_col, std::vector<int>& queens)
{
    std::vector<int> attacks_in_col(n, 0);
    std::vector<int> attacks_in_diagonal_pos(n * 2 - 1, 0);
    std::vector<int> attacks_in_diagonal_neg(n * 2 - 1, 0);

    // For each starting column
    for (int start_col = min_col; start_col < max_col; start_col += 2)
    {
        // Check if any thread has completed
        complete_lock.lock();
        if (first_complete_thread != -1)
        {
            complete_lock.unlock();
            return;
        }
        complete_lock.unlock();

        // Reset
        std::fill(attacks_in_col.begin(), attacks_in_col.end(), 0);
        std::fill(attacks_in_diagonal_pos.begin(), attacks_in_diagonal_pos.end(), 0);
        std::fill(attacks_in_diagonal_neg.begin(), attacks_in_diagonal_neg.end(), 0);

        // Place all the queens in the starting column
        for (int row = 0; row < n; row++)
        {
            place_queen(row, start_col, queens, attacks_in_col, attacks_in_diagonal_pos, attacks_in_diagonal_neg);
        }

        // Repeat for max_iterations
        for (int i = 0; i < max_iterations; i++)
        {
            // The sum of the number of attacks on each queen
            int attacks = 0;

            // For each queen
            for (int row = 0; row < n; row++)
            {
                // Remove it to avoid self collision
                remove_queen(row, queens[row], attacks_in_col, attacks_in_diagonal_pos, attacks_in_diagonal_neg);

                // Get the position in the row which the least number of queens can attack
                int min_col    = 0;
                int min_attack = get_num_attacks(row, 0, attacks_in_col, attacks_in_diagonal_pos, attacks_in_diagonal_neg);
                for (int col = 1; col < n; col++)
                {
                    int attacks = get_num_attacks(row, col, attacks_in_col, attacks_in_diagonal_pos, attacks_in_diagonal_neg);
                    if (attacks < min_attack)
                    {
                        min_col    = col;
                        min_attack = attacks;
                    }
                }
                // Place the queen in the new position
                place_queen(row, min_col, queens, attacks_in_col, attacks_in_diagonal_pos, attacks_in_diagonal_neg);

                // Sum the attacks on each queen
                attacks += min_attack;
            }
            // If the sum of all the attack equal zero we've found a solution
            if (attacks == 0)
            {
                // Mark the thread as complete
                complete_lock.lock();
                if (first_complete_thread == -1)
                {
                    first_complete_thread = thread_id;
                }
                complete_lock.unlock();
                return;
            }
        }
    }
    // In we found no solution
    // Mark the thread as complete
    complete_lock.lock();
    completed_threads++;
    complete_lock.unlock();
    return;
}

int main(int argc, char** argv)
{
    // Get the size of the board.
    int n = get_argument(argc, argv, 10000);
    if (n == -1)
    {
        std::cout << "Usage: n-queens N\n Where N is a number in the range [1-10000]\n";
        return EXIT_FAILURE;
    }

    // The required offset into the attacks_in_diagonal_neg vector as the diagonal is calculated as col-row and we don't want negative indices.
    neg_offset = n;

    // The number of times to move the queens before trying a different starting column.
    int max_iterations = 15;

    // While having more threads than CPU cores doesn't preform commutations faster, it allows trying more starting
    // positions at once. The ones that preform better will complete first.
    int num_threads = std::min(n - 1, 200);

    // Start columns per thread
    int cpt = n / num_threads;

    // Divide the starting columns
    std::vector<int> start_pos;
    int pos_count = 0;
    for (int i = 0; i < num_threads; i++)
    {
        start_pos.push_back(pos_count);
        pos_count += cpt;
    }
    start_pos.push_back(n);

    // Start the threads
    std::vector<std::thread> threads;
    std::vector<std::vector<int>> thread_queens(num_threads);
    for (int i = 0; i < num_threads; i++)
    {
        thread_queens[i].resize(n);
        threads.emplace_back(
          std::thread(solve, i, n, max_iterations, start_pos[i], start_pos[i + 1] - 1, std::ref(thread_queens[i])));
    }

    // Wait until one found a solution or the threads have tried all starting positions.
    while (true)
    {
        complete_lock.lock();
        if (first_complete_thread != -1 || completed_threads >= num_threads)
        {
            complete_lock.unlock();
            break;
        }
        complete_lock.unlock();
    }

    // Join all the threads to keep things neat.
    for (int i = 0; i < num_threads; i++)
    {
        threads[i].join();
    }

    // Get the solution from one of the threads.
    auto queens = thread_queens[first_complete_thread != -1 ? first_complete_thread : 0];

    // Print
    auto board = queens_to_board(queens);
    if (validate_board(board))
    {
        // print_board(board);
        std::cout << "Found a solution: \n";
    }
    else
    {
        std::cout << "Produced an invalid solution: \n";
    }

    return EXIT_SUCCESS;
}
