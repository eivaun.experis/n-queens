#include <iostream>

#include "utility.h"

class Board
{
  private:
    int n;
    std::vector<std::vector<int>> queens;

  public:
    // Initialise a board of size n*n with zeroes
    Board(int n) : n(n)
    {
        queens.resize(n);
        for (auto& row : queens)
        {
            row.resize(n, 0);
        }
    }

    // Gets the size of the board
    inline int size() const { return n; }

    // Removes the queen in the given position
    void remove_queen(int row, int col) { queens[row][col] = false; }

    const std::vector<std::vector<int>>& get_board() const { return queens; }

    // Tries to place a queen and checks for conflicts
    bool try_place_queen(int row, int col)
    {
        // Check the above row
        for (int r = 0; r < row; r++)
        {
            if (queens[r][col])
            {
                return false;
            }
        }

        // Check diagonally up right
        for (int r = row, c = col; r >= 0 && c < n; r--, c++)
        {
            if (queens[r][c])
            {
                return false;
            }
        }

        // Check diagonally up left
        for (int r = row, c = col; r >= 0 && c >= 0; r--, c--)
        {
            if (queens[r][c])
            {
                return false;
            }
        }
        queens[row][col] = true;
        return true;
    }
};

// Places queens recursively, starting with the top row
bool place_queens_rec(Board& b, int row)
{
    // Place the new queen in each square of the row
    for (int col = 0; col < b.size(); col++)
    {
        // If We can place it in this square
        if (b.try_place_queen(row, col))
        {
            // Check if we've placed all the queens
            if (row == b.size() - 1)
            {
                return true;
            }
            // Go on to the next queen
            else if (place_queens_rec(b, row + 1))
            {
                return true;
            }
            else
            {
                // If the following queens could not be placed, we must try a new position for this one
                b.remove_queen(row, col);
            }
        }
    }

    // The queen could not be placed in any square in this row
    return false;
}

int main(int argc, char** argv)
{
    int n = get_argument(argc, argv, 10);
    if (n == -1)
    {
        std::cout << "Usage: n-queens-naive N\n Where N is a number in the range [1-10]\n";
        return EXIT_FAILURE;
    }

    // Setup a n*n board
    Board board(n);

    // Try to find a solution
    if (place_queens_rec(board, 0))
    {
        std::cout << "Found a solution:\n";
        print_board(board.get_board());
    }
    else
    {
        std::cout << "Failed to find a solution!\n";
    }

    return EXIT_SUCCESS;
}
