#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

#include "utility.h"

// The order of which column/row we try to place the queens in first
std::vector<int> index_order;

// The position of each queen.
std::vector<int> queens;

// Keeps track of the occupied columns.
// Column 2 is occupied if occupied_cols[2]==true
// Using int as vector<bool> is very slow
std::vector<int> occupied_cols;

std::vector<int> attacks_in_col;
std::vector<int> attacks_in_diagonal_pos;
std::vector<int> attacks_in_diagonal_neg;
int neg_offset = 0;

int get_num_attacks(int row, int col)
{
    int attacks = 0;
    attacks += attacks_in_diagonal_pos[col + row];
    attacks += attacks_in_diagonal_neg[col - row + neg_offset];
    return attacks;
}

void place_queen(int row, int col)
{
    queens[row]        = col;
    occupied_cols[col] = true;
    attacks_in_diagonal_pos[col + row]++;
    attacks_in_diagonal_neg[col - row + neg_offset]++;
}

void remove_queen(int row, int col)
{
    occupied_cols[col] = false;
    attacks_in_diagonal_pos[col + row]--;
    attacks_in_diagonal_neg[col - row + neg_offset]--;
}

// Places queens recursively, starting with the top row
bool place_queens_rec(int n, int depth)
{
    // Check if we've placed all the queens
    if (depth == n) return true;

    // Edges -> middle
    int row = index_order[n - depth - 1];

    // Place the new queen in each square of the row
    for (int c = 0; c < n; c++)
    {
        // Middle -> edges
        int col = index_order[c];

        // Can't have two queens in the some column
        if (occupied_cols[col]) continue;

        // If we can place it in this square
        if (get_num_attacks(row, col) == 0)
        {
            place_queen(row, col);
            // Go on to the next queen
            if (place_queens_rec(n, depth + 1))
            {
                return true;
            }
            // If the following queens could not be placed, we must try a new position for this one
            remove_queen(row, col);
            occupied_cols[col] = false;
        }
    }

    // The queen could not be placed in any square in this row
    return false;
}

int main(int argc, char** argv)
{
    // Get the size of the board
    int n = get_argument(argc, argv, 1000);
    if (n == -1)
    {
        std::cout << "Usage: n-queens N\n Where N is a number in the range [1-100]\n";
        return EXIT_FAILURE;
    }

    // Setup
    queens.resize(n);
    attacks_in_col.resize(n, 0);
    attacks_in_diagonal_pos.resize(n * 2 - 1, 0);
    attacks_in_diagonal_neg.resize(n * 2 - 1, 0);
    neg_offset = n;
    occupied_cols.resize(n, false);

    index_order.resize(n);
    std::iota(index_order.begin(), index_order.end(), 0);
    for (int i = 0; i < n; i++)
    {
        std::swap(index_order[i], index_order[rand()%n]);
    }

    // Try to find a solution
    if (!place_queens_rec(n, 0))
    {
        std::cout << "Failed to find a solution!\n";
    }
    else
    {
        auto board = queens_to_board(queens);
        if (validate_board(board))
        {
            print_board(board);
            std::cout << "Found a solution: \n";
        }
        else
        {
            std::cout << "Produced a invalid solution: \n";
        }
    }

    return EXIT_SUCCESS;
}
