#pragma once

#include <sstream>
#include <iostream>
#include <vector>

struct pos
{
    int row, col;
    pos() = default;
    pos(int row, int col) : row(row), col(col) {}
};

// Try to read n from the command line arguments
int get_argument(int argc, char** argv, size_t max_n)
{
    // Check if we have an argument
    if (argc < 2) return -1;

    // Try to read the argument as an unsigned number
    size_t n = 0;
    std::istringstream iss(argv[1]);
    if (!(iss >> n)) return -1;

    // Check if N is in the valid range
    if (n < 1 || n > max_n) return -1;

    return n;
}

std::vector<std::vector<int>> queens_to_board(const std::vector<pos>& queen_positions)
{
    int n = queen_positions.size();
    std::vector<std::vector<int>> board;
    board.resize(n);
    for (auto& row : board)
    {
        row.resize(n, 0);
    }

    for (const auto& pos : queen_positions)
    {
        board[pos.row][pos.col] = true;
    }

    return board;
}

std::vector<std::vector<int>> queens_to_board(const std::vector<int>& queen_columns)
{
    int n = queen_columns.size();
    std::vector<std::vector<int>> board;
    board.resize(n);
    for (auto& row : board)
    {
        row.resize(n, 0);
    }

    for (int r = 0; r < n; r++)
    {
        board[r][queen_columns[r]] = true;
    }

    return board;
}

bool validate_board(std::vector<std::vector<int>>& board)
{
    int n = board.size();

    for (int r = 0; r < n; r++)
    {
        for (int c = 0; c < n; c++)
        {
            // Skip if not a queen
            if(!board[r][c]) 
            continue;

            // Remove the current queen to simplify checking
            board[r][c] = false;

            // Check row
            for (int rr = 0; rr < n; rr++)
            {
                if(board[rr][c]) 
                return false;
            }

            // Check column
            for (int cc = 0; cc < n; cc++)
            {
                if(board[r][cc]) 
                return false;
            }

            // Check diagonally down right
            for (int rr = r, cc = c; rr < n && cc < n; rr++, cc++)
            {
                if(board[rr][cc]) 
                return false;
            }

            // Check diagonally down left
            for (int rr = r, cc = c; rr < n && cc >= 0; rr++, cc--)
            {
                if(board[rr][cc]) 
                return false;
            }
            

            // Check diagonally up right
            for (int rr = r, cc = c; rr >= 0 && cc < n; rr--, cc++)
            {
                if(board[rr][cc]) 
                return false;
            }

            // Check diagonally up left
            for (int rr = r, cc = c; rr >= 0 && cc >= 0; rr--, cc--)
            {
                if(board[rr][cc]) 
                return false;
            }

            // Put the queen back in place
            board[r][c] = true;

            // No more queens in this row
            break; 
        }
    }
    return true;
}

void print_board(const std::vector<std::vector<int>>& board)
{
    // Top border
    int n = board.size();
    std::cout << "┌─";
    for (int i = 0; i < n; i++)
    {
        std::cout << "──";
    }
    std::cout << "┐\n";

    // Rows
    for (int r = 0; r < n; r++)
    {
        std::cout << "│ ";
        for (int c = 0; c < n; c++)
        {
            std::cout << (board[r][c] ? "Q " : ". ");
        }
        std::cout << "│\n";
    }

    // Bottom border
    std::cout << "└─";
    for (int i = 0; i < n; i++)
    {
        std::cout << "──";
    }
    std::cout << "┘\n";
}

void print_board_fancy(const std::vector<std::vector<int>>& board)
{
    // Top border
    int n = board.size();
    std::cout << "┌";
    for (int i = 0; i < n - 1; i++)
    {
        std::cout << "───┬";
    }
    std::cout << "───┐\n";

    // Rows
    for (int r = 0; r < n; r++)
    {
        for (int c = 0; c < n; c++)
        {
            std::cout << "│" << (board[r][c] ? " Q " : "   ");
        }
        std::cout << "│\n";

        if (r != n - 1)
        {
            std::cout << "├";
            for (int i = 0; i < n - 1; i++)
            {
                std::cout << "───┼";
            }
            std::cout << "───┤\n";
        }
    }

    // Bottom border
    std::cout << "└";
    for (int i = 0; i < n - 1; i++)
    {
        std::cout << "───┴";
    }
    std::cout << "───┘\n";
}
